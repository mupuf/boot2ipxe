CROSS_COMPILE ?= aarch64-none-elf-
UBOOT_ARCH ?= arm

IPXE_TARGET ?= bin-arm64-efi/snp--ecm--ncm--acm.efi
IPXE_CONFIG_DIR = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))/arch/arm64/ipxe-config

ifeq (, $(shell which $(CROSS_COMPILE)gcc))
    $(warning The $(CROSS_COMPILE) toolchain is missing)

    CROSS_COMPILE = aarch64-linux-gnu-
    ifeq (, $(shell which $(CROSS_COMPILE)gcc))
        $(error The $(CROSS_COMPILE) toolchain is missing)
    else
        $(warning Using the $(CROSS_COMPILE) toolchain)
    endif
endif

.PHONY: test
test:
	$(error test is not implemented)
