include $(ROOTDIR)/Makefile.util.mk

UBOOT_PATH := $(ROOTDIR)/dependencies/u-boot

UBOOT_SCRIPT ?= $(UBOOT_PATH)/boot.cmd
UBOOT_BIN_NAME ?= u-boot.bin
UBOOT_REPO_URL ?= https://github.com/u-boot/u-boot.git

# v2023.07.02
UBOOT_COMMIT ?= 83cdab8b2c6ea0fc0860f8444d083353b47f1d5c

$(eval $(call GIT_REPO_DEPENDENCY,UBOOT))

$(eval $(call DEPENDABLE_VAR,UBOOT_ARCH))
$(eval $(call DEPENDABLE_VAR,UBOOT_SCRIPT))
$(eval $(call DEPENDABLE_VAR,UBOOT_TARGET))
$(eval $(call DEPENDABLE_VAR,UBOOT_BIN_NAME))
$(eval $(call DEPENDABLE_VAR,UBOOT_CONFIG_FORCE))
$(eval $(call DEPENDABLE_VAR,CONSOLE_BAUDRATE))
$(eval $(call DEPENDABLE_VAR,CROSS_COMPILE))
$(eval $(call DEPENDABLE_VAR,ROCKCHIP_TPL))
$(eval $(call DEPENDABLE_VAR,BL31))

# NOTE: It is OK to add empty dependencies
$(UBOOT_BIN_INSTALL_PATH): $(ROOTDIR)/Makefile.util.mk $(UBOOT_PATH)/Makefile.mk $(BL31) $(ROCKCHIP_TPL) $(ARG~BL31) $(ARG~ROCKCHIP_TPL) $(ARG~UBOOT_ARCH) $(ARG~UBOOT_BIN_NAME) $(ARG~CROSS_COMPILE) $(ARG~UBOOT_TARGET) $(ARG~UBOOT_CONFIG_FORCE) $(UBOOT_SRC)
	$(call fail-if-any-var-unset,UBOOT_TARGET CROSS_COMPILE UBOOT_ARCH)
	$(MAKE) -C $(UBOOT_SRC) ARCH=$(UBOOT_ARCH) CROSS_COMPILE=$(CROSS_COMPILE) $(UBOOT_TARGET)

	# Common tweaks to the u-boot config
	echo "CONFIG_BOOTDELAY=0" >> $(UBOOT_SRC)/.config
	echo "CONFIG_BAUDRATE=$(CONSOLE_BAUDRATE)" >> $(UBOOT_SRC)/.config

	# Apply the wanted config changes
	for cfg in $(UBOOT_CONFIG_FORCE); do echo $$cfg >> $(UBOOT_SRC)/.config; done

	$(MAKE) -C $(UBOOT_SRC) ARCH=$(UBOOT_ARCH) CROSS_COMPILE=$(CROSS_COMPILE) oldconfig
	$(MAKE) -C $(UBOOT_SRC) ARCH=$(UBOOT_ARCH) CROSS_COMPILE=$(CROSS_COMPILE) BL31=$(BL31) ROCKCHIP_TPL=$(ROCKCHIP_TPL)

	mkdir -p `dirname $@`
	cp $(UBOOT_SRC)/$(UBOOT_BIN_NAME) $@

# Note: we depend on UBOOT_BIN_INSTALL_PATH because otherwise the mkimage binary will be
# missing, since it has not been compiled yet
$(UBOOT_SCR_INSTALL_PATH): $(UBOOT_PATH)/Makefile.mk $(UBOOT_BIN_INSTALL_PATH) $(UBOOT_SCRIPT) $(ARG~UBOOT_SCRIPT) $(ARG~UBOOT_ARCH)
	$(UBOOT_SRC)/tools/mkimage -A $(UBOOT_ARCH) -T script -d $(UBOOT_SCRIPT) $(UBOOT_SCR_INSTALL_PATH)
