include $(ROOTDIR)/Makefile.util.mk

RPI_FW_PATH := $(ROOTDIR)/dependencies/rpi-firmware
RPI_FW_REPO_URL ?= https://github.com/raspberrypi/firmware.git
RPI_FW_COMMIT ?= 83dafbc92c0e63f76ca2ecdd42462d56489d1c77
RPI_FW_LIST ?= \
	bcm*.dtb \
	overlays/ \
	bootcode.bin \
	fixup4.dat \
	fixup.dat \
	start4.elf \
	start.elf

$(eval $(call DEPENDABLE_VAR,RPI_FW_LIST))
$(eval $(call DEPENDABLE_VAR,RPI_FW_INSTALL_PREFIX))

$(eval $(call GIT_REPO_DEPENDENCY,RPI_FW))

# NOTE: We can't easily detect if any of the firmware files changed, so let's instead
# rebuild if the source folder changed, or if the list of files changed
.PHONY: rpi-firmware
rpi-firmware: $(ROOTDIR)/Makefile.util.mk $(RPI_FW_PATH)/Makefile.mk $(RPI_FW_SRC) $(ARG~RPI_FW_LIST) $(ARG~RPI_FW_INSTALL_PREFIX)
	$(call fail-if-any-var-unset,RPI_FW_INSTALL_PREFIX)
	mkdir -p $(RPI_FW_INSTALL_PREFIX)
	$(foreach FW,$(RPI_FW_LIST),$(shell cp -r $(RPI_FW_SRC)/boot/$(FW) $(RPI_FW_INSTALL_PREFIX)/))
