include $(ROOTDIR)/Makefile.util.mk

# path to dep/ipxe
IPXE_PATH := $(ROOTDIR)/dependencies/ipxe

IPXE_SCRIPT ?= $(IPXE_PATH)/embed.ipxe
IPXE_CONFIG_DIR ?= $(IPXE_PATH)/config

IPXE_REPO_URL ?= https://github.com/ipxe/ipxe.git
IPXE_COMMIT ?= 8fc11d8a4ad41f15af3d081250865f971312d871
IPXE_GIT_FETCH_OPTS := --tags

.PHONY: ipxe
ipxe: $(IPXE_INSTALL_PATH)

# do not build ipxe if it is provided
# IPXE_BIN should be a absolute path
ifdef IPXE_BIN

$(IPXE_INSTALL_PATH): $(IPXE_BIN)
	cp $< $@

else

$(eval $(call GIT_REPO_DEPENDENCY,IPXE))

$(eval $(call DEPENDABLE_VAR,IPXE_SCRIPT))
$(eval $(call DEPENDABLE_VAR,IPXE_EXTRA_ARGS))
$(eval $(call DEPENDABLE_VAR,IPXE_TARGET))
$(eval $(call DEPENDABLE_VAR,CROSS_COMPILE))

$(IPXE_INSTALL_PATH): $(ROOTDIR)/Makefile.util.mk $(IPXE_PATH)/Makefile.mk $(IPXE_SRC) $(IPXE_CONFIG_DIR) $(IPXE_SCRIPT) $(ARG~IPXE_SCRIPT) $(ARG~IPXE_EXTRA_ARGS) $(ARG~IPXE_TARGET) $(ARG~CROSS_COMPILE)
	$(call fail-if-any-var-unset,IPXE_TARGET CROSS_COMPILE)
	cp $(IPXE_CONFIG_DIR)/* $(IPXE_SRC)/src/config/local/
	embed_script=$(shell mktemp); \
	cat $(IPXE_SCRIPT) | sed -e "s/\$${version}/$(shell git describe --dirty --always --tags)/" > $$embed_script; \
	$(MAKE) -C $(IPXE_SRC)/src CROSS_COMPILE=$(CROSS_COMPILE) EMBED=$$embed_script $(IPXE_EXTRA_ARGS) $(IPXE_TARGET) NO_WERROR=1; \
	rm $$embed_script
	mkdir -p `dirname $@`
	cp $(IPXE_SRC)/src/$(IPXE_TARGET) $@

endif
