include Makefile.util.mk

export ROOTDIR = $(CURDIR)
export TARGET = $(MAKECMDGOALS)
export OUT = $(ROOTDIR)/output
export TARGET_OUT ?= $(OUT)/$(TARGET)/out
export TARGET_TMP ?= $(OUT)/$(TARGET)/tmp

export CONSOLE_BAUDRATE ?= 115200

$(OUT) $(TARGET_OUT) $(TARGET_TMP):
	mkdir -p $@

.PHONY: _basedeps
_basedeps: $(OUT) $(TARGET_OUT) $(TARGET_TMP)

.PHONY: clean
clean:
	rm -rf $(OUT) || true

.PHONY: veryclean
veryclean: clean
	rm -rf dependencies/*/src || true

.PHONY: test
test:
	$(call fail-if-any-var-unset,TARGET)
	$(MAKE) -C target/$(TARGET) test

generic_x86_64: _basedeps  ## A BIOS- and EFI-compatible disk image for x86_64 platforms
	$(MAKE) -C target/$@ build

efi_x86_64: _basedeps  ## An EFI-compatible disk image for x86_64 platforms
	$(MAKE) -C target/generic_efi build ARCH=x86_64

efi_arm64: _basedeps  ## An EFI-compatible disk image for ARM64 platforms
	$(MAKE) -C target/generic_efi build ARCH=arm64

rpi_arm64: _basedeps  ## SD-card image compatible with Raspberry Pi 2-4
	$(MAKE) -C target/$@ build

nanopi_r4s: _basedeps  ## An SD-card image for FriendlyELEC's NanoPi R4S
	$(MAKE) -C target/rockchip RK_BOARD=nanopi_r4s build

nanopi_r5c: _basedeps ## An SD-card image for FriendlyELEC's NanoPi R5C
	$(MAKE) -C target/rockchip RK_BOARD=nanopi_r5c build

nanopi_r6s: _basedeps  ## An SD-card image for FriendlyELEC's NanoPi R6S
	$(MAKE) -C target/rockchip RK_BOARD=nanopi_r6s build

rock_5b: _basedeps  ## An SD-card image for Radxa's Rock 5B
	$(MAKE) -C target/rockchip RK_BOARD=rock_5b build

list-targets:
	@awk 'BEGIN {FS = ":.*##"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "%s -%s\n", $$1, $$2 } /^## @/' $(firstword $(MAKEFILE_LIST))
