UBOOT_COMMIT = 9a48ec3e91c6fbdb8d67dfd80488def8fa61b681
UBOOT_REPO_URL = https://github.com/u-boot/u-boot.git
UBOOT_TARGET=nanopi-r6s-rk3588s_defconfig
UBOOT_CONFIG_FORCE = CONFIG_ROCKCHIP_SERIAL=y CONFIG_PCI_INIT_R=y

include $(ROOTDIR)/dependencies/rkbin/Makefile.mk

ROCKCHIP_TPL = $(RKBIN_SRC)/bin/rk35/rk3588_ddr_lp4_2112MHz_lp5_2736MHz_v1.12.bin
$(ROCKCHIP_TPL): $(RKBIN_SRC)
	@true

BL31 = $(RKBIN_SRC)/bin/rk35/rk3588_bl31_v1.40.elf
$(BL31): $(RKBIN_SRC)
	@true
