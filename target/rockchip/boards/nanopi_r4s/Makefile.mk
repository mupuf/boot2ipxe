UBOOT_TARGET=nanopi-r4s-rk3399_defconfig
UBOOT_CONFIG_FORCE = CONFIG_ROCKCHIP_SERIAL=y

ATF_PLAT := rk3399
ATF_PATCHES := $(PWD)/target/rockchip/boards/$(RK_BOARD)/atf/
include $(ROOTDIR)/dependencies/arm-trusted-firmware/Makefile.mk
BL31 = $(ATF_BL31_PATH)
