UBOOT_COMMIT = c1710bfc4fa05cf694a62a31bfc549b03a4b36e4
UBOOT_TARGET = rock5b-rk3588_defconfig
UBOOT_CONFIG_FORCE = CONFIG_ROCKCHIP_SERIAL=y

include ../../dependencies/rkbin/Makefile.mk

ROCKCHIP_TPL = $(RKBIN_SRC)/bin/rk35/rk3588_ddr_lp4_2112MHz_lp5_2736MHz_v1.12.bin
$(ROCKCHIP_TPL): $(RKBIN_SRC)

BL31 = $(RKBIN_SRC)/bin/rk35/rk3588_bl31_v1.40.elf
$(BL31): $(RKBIN_SRC)
