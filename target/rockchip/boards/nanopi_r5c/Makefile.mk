UBOOT_COMMIT = 9a48ec3e91c6fbdb8d67dfd80488def8fa61b681
UBOOT_REPO_URL = https://github.com/u-boot/u-boot.git
UBOOT_TARGET=nanopi-r5c-rk3568_defconfig
UBOOT_CONFIG_FORCE = CONFIG_ROCKCHIP_SERIAL=y CONFIG_PCI_INIT_R=y

include $(ROOTDIR)/dependencies/rkbin/Makefile.mk

ROCKCHIP_TPL = $(RKBIN_SRC)/bin/rk35/rk3568_ddr_1560MHz_v1.18.bin
$(ROCKCHIP_TPL): $(RKBIN_SRC)
	@true

BL31 = $(RKBIN_SRC)/bin/rk35/rk3568_bl31_v1.43.elf
$(BL31): $(RKBIN_SRC)
	@true
